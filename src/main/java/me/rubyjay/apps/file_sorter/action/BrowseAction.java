package me.rubyjay.apps.file_sorter.action;

import me.rubyjay.apps.file_sorter.file_type.FileType;
import me.rubyjay.apps.file_sorter.forms.MainForm;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.io.File;

import static javax.swing.JFileChooser.CANCEL_OPTION;
import static javax.swing.JFileChooser.ERROR_OPTION;
import static javax.swing.JOptionPane.ERROR_MESSAGE;

public class BrowseAction implements Action {

    @Nullable
    @Override
    public Icon getIcon() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "Click here to choose a file...";
    }

    @Override
    public boolean matchesType(FileType type) {
        return true;
    }

    @Override
    public boolean execute(MainForm mainForm, FileType type, File file) {
        execute(mainForm);
        return false;
    }

    public static void execute(MainForm mainForm) {
        File file;
        final JFileChooser fc = new JFileChooser();

        int res;
        do {
            res = fc.showOpenDialog(mainForm);
        } while (res == ERROR_OPTION);

        if (res == CANCEL_OPTION) {
            mainForm.selectNoFile();
            return;
        }

        file = fc.getSelectedFile();

        if (file == null || !file.exists()) {
            JOptionPane.showMessageDialog(mainForm, "File does not exist.", "Error", ERROR_MESSAGE);
            mainForm.selectNoFile();
            return;
        }

        mainForm.selectFile(file);
    }

}
