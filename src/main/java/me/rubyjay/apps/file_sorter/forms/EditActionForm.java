package me.rubyjay.apps.file_sorter.forms;

import me.rubyjay.apps.file_sorter.FileSorterApp;
import me.rubyjay.apps.file_sorter.action.BasicAction;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.function.Consumer;

public abstract class EditActionForm extends JDialog {

    private final int index;
    private final BasicAction action;

    private JPanel panelMain;
    private JButton btnIcon;
    private JLabel lblDisplayName;
    private JTextField txtDisplayName;
    private JLabel lblDestination;
    private JTextField txtDestination;
    private JButton btnDestinationBrowse;
    private JLabel lblCommand;
    private JTextField txtCommand;
    private JButton btnCommandBrowse;
    private JButton btnDelete;
    private JPanel panelButtons;
    private JButton btnOK;
    private JButton btnCancel;

    public EditActionForm(MainForm owner, int index, BasicAction action, boolean isEditing) {
        super(owner, isEditing ? "Edit Action" : "Add Action", ModalityType.DOCUMENT_MODAL);
        this.index = index;
        this.action = action;

        final DocumentListener documentListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                validateOK();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                validateOK();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                validateOK();
            }
        };

        panelMain = new JPanel(new GridBagLayout());
        this.setContentPane(panelMain);

        btnIcon = new JButton();
        btnIcon.setIcon(action.getIcon());
        btnIcon.setPreferredSize(new Dimension(100, 100));
        add(panelMain, btnIcon, c -> {
            c.gridx = 0;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 3;
            c.insets = new Insets(5, 5, 5, 5);
            c.anchor = GridBagConstraints.PAGE_START;
        });

        lblDisplayName = new JLabel("Display Name");
        lblDisplayName.setHorizontalAlignment(JLabel.LEFT);
        add(panelMain, lblDisplayName, c -> {
            c.gridx = 1;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        txtDisplayName = new JTextField();
        txtDisplayName.setText(action.getDisplayName());
        txtDisplayName.getDocument().addDocumentListener(documentListener);
        add(panelMain, txtDisplayName, c -> {
            c.gridx = 2;
            c.gridy = 0;
            c.gridwidth = 2;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        lblDestination = new JLabel("Destination");
        lblDestination.setHorizontalAlignment(JLabel.LEFT);
        add(panelMain, lblDestination, c -> {
            c.gridx = 1;
            c.gridy = 1;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        txtDestination = new JTextField();
        txtDestination.setText(action.getDestination());
        txtDestination.setPreferredSize(new Dimension(250, 27));
        txtDestination.getDocument().addDocumentListener(documentListener);
        add(panelMain, txtDestination, c -> {
            c.gridx = 2;
            c.gridy = 1;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
        });

        btnDestinationBrowse = new JButton("Browse...");
        btnDestinationBrowse.addActionListener(e -> {
            final JFileChooser fc = new JFileChooser();

            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            if (!txtDestination.getText().isEmpty()) {
                fc.setSelectedFile(new File(txtDestination.getText()));
            }
            if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                txtDestination.setText(fc.getSelectedFile().getAbsolutePath());
            }
        });
        add(panelMain, btnDestinationBrowse, c -> {
            c.gridx = 3;
            c.gridy = 1;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        lblCommand = new JLabel("Command");
        lblCommand.setHorizontalAlignment(JLabel.LEFT);
        add(panelMain, lblCommand, c -> {
            c.gridx = 1;
            c.gridy = 2;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        txtCommand = new JTextField();
        txtCommand.setText(action.getCommand());
        txtCommand.setPreferredSize(new Dimension(250, 27));
        txtCommand.getDocument().addDocumentListener(documentListener);
        add(panelMain, txtCommand, c -> {
            c.gridx = 2;
            c.gridy = 2;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
        });

        btnCommandBrowse = new JButton("Browse...");
        add(panelMain, btnCommandBrowse, c -> {
            c.gridx = 3;
            c.gridy = 2;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        if (isEditing) {
            btnDelete = new JButton("Delete");
            btnDelete.addActionListener(e -> {
                final int result = JOptionPane.showConfirmDialog(this,
                        "Are you sure you want to delete this?",
                                "Confirm delete",
                                JOptionPane.YES_NO_OPTION);

                if (result == 0 && onDelete(index, this.action)) {
                    FileSorterApp.loadAllConfigs();
                    owner.selectFile(owner.getSelectedFile());
                    this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
                }
            });
            add(panelMain, btnDelete, c -> {
                c.gridx = 0;
                c.gridy = 3;
                c.gridwidth = 1;
                c.gridheight = 1;
                c.insets = new Insets(5, 5, 5, 5);
                c.anchor = GridBagConstraints.LINE_START;
            });
        }

        panelButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        add(panelMain, panelButtons, c -> {
            c.gridx = 1;
            c.gridy = 3;
            c.gridwidth = 3;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        btnOK = new JButton("OK");
        btnOK.addActionListener(e -> {
            this.action.setIcon(btnIcon.getIcon());
            this.action.setDisplayName(txtDisplayName.getText());
            this.action.setDestination(txtDestination.getText());
            this.action.setCommand(txtCommand.getText());
            if (onSubmit(this.action)) {
                FileSorterApp.loadAllConfigs();
                owner.selectFile(owner.getSelectedFile());
                this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            }
        });
        SwingUtilities.getRootPane(this).setDefaultButton(btnOK);
        panelButtons.add(btnOK);

        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(e -> this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)));
        panelButtons.add(btnCancel);

        validateOK();

        this.pack();
        this.setLocationRelativeTo(owner);
        this.setVisible(true);
    }

    private void add(JPanel panel, JComponent component, Consumer<GridBagConstraints> gridBagConstraintsConsumer) {
        final GridBagConstraints c = new GridBagConstraints();
        
        gridBagConstraintsConsumer.accept(c);
        panel.add(component, c);
    }

    private void validateOK() {
        boolean valid = !txtDisplayName.getText().isEmpty()
                && (!txtDestination.getText().isEmpty() || !txtCommand.getText().isEmpty());

        btnOK.setEnabled(valid);
    }

    public abstract boolean onSubmit(BasicAction action);

    public abstract boolean onDelete(int index, BasicAction action);

}
