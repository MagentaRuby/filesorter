package me.rubyjay.apps.file_sorter.forms.settings_panes;

import me.rubyjay.apps.file_sorter.file_type.FileType;
import me.rubyjay.apps.file_sorter.forms.SettingsForm;

import javax.swing.*;
import java.awt.*;

public class FileTypesPane extends JPanel {

    private final SettingsForm settingsForm;

    private JScrollPane scrollFileTypes;
    private JList<FileType> listFileTypes;

    public FileTypesPane(SettingsForm settingsForm) {
        this.settingsForm = settingsForm;

        this.setLayout(new BorderLayout());


    }

}
