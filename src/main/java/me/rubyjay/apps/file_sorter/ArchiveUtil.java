package me.rubyjay.apps.file_sorter;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public final class ArchiveUtil {

    private ArchiveUtil() {}


    public static boolean containsFileNames(File file, List<String> archiveContains) {
        try {
            final List<String> fileNames = new ZipFile(file).stream()
                    .map(ZipEntry::getName)
                    .collect(Collectors.toList());

            archiveLoop:
            for (String aName : archiveContains) {
                for (String fName : fileNames) {
                    if (fName.matches(aName)) continue archiveLoop;
                }
                return false;
            }

            return true;

        } catch (IOException ex) {
            FileSorterApp.handleException(ex);
            return false;
        }
    }

}
