package me.rubyjay.apps.file_sorter.action;

import me.rubyjay.apps.file_sorter.file_type.FileType;
import me.rubyjay.apps.file_sorter.forms.MainForm;
import ninja.leaping.configurate.ConfigurationNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.io.File;

public interface Action {

    static Action load(String fileTypeID, ConfigurationNode c) {
        final Icon icon;
        final ConfigurationNode iconNode = c.getNode("icon");

        if (iconNode.isEmpty()) {
            icon = null;
        } else {
            final String iconString = iconNode.getString("");

            if (iconString.isEmpty()) {
                icon = null;
            } else {
                icon = new ImageIcon(iconString);
            }
        }

        final String displayName = c.getNode("display-name").getString("");
        final String destination = c.getNode("destination").getString("");
        final String command = c.getNode("command").getString("");

        boolean showDestination = c.getNode("show-destination").getBoolean(false);
        boolean showCommand = c.getNode("show-command").getBoolean(false);

        return new BasicAction(fileTypeID, icon, displayName, destination, command, showDestination, showCommand);
    }

    static ConfigurationNode export(BasicAction action, ConfigurationNode emptyNode) {
        emptyNode.getNode("display-name").setValue(action.getDisplayName());
        if (action.hasDestination()) {
            emptyNode.getNode("destination").setValue(action.getDestination());
        }
        if (action.hasCommand()) {
            emptyNode.getNode("command").setValue(action.getCommand());
        }
        if (action.showDestination()) {
            emptyNode.getNode("show-destination").setValue(action.showDestination());
        }
        if (action.showCommand()) {
            emptyNode.getNode("show-command").setValue(action.showCommand());
        }
        return emptyNode;
    }

    @Nullable
    Icon getIcon();

    @NotNull
    String getText();

    @NotNull
    default String getTooltip() {
        return getText();
    }

    boolean matchesType(FileType type);

    boolean execute(MainForm mainForm, FileType type, File file);

}
