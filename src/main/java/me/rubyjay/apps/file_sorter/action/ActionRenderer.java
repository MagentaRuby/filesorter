package me.rubyjay.apps.file_sorter.action;

import javax.swing.*;
import java.awt.*;


public class ActionRenderer implements ListCellRenderer<Action> {

    private final Color panelBackground;
    private final Color selectionBackground;
    private final Color selectionForeground;

    public ActionRenderer() {
        this.panelBackground = UIManager.getColor("Panel.background");
        this.selectionBackground = UIManager.getColor("List.selectionBackground");
        this.selectionForeground = UIManager.getColor("List.selectionForeground");
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Action> jList, Action action, int i, boolean isSelected, boolean cellHasFocus) {
        if (action instanceof DummyAction) {
            final JLabel lbl = new JLabel("");
            final Dimension size = new Dimension(0, 0);

            lbl.setMinimumSize(size);
            lbl.setMaximumSize(size);
            lbl.setPreferredSize(size);

            return lbl;
        }

        final JButton btn = new JButton(action.getText(), action.getIcon());

        btn.setToolTipText(action.getTooltip());

        if (action instanceof AddAction) {
            btn.setFont(new Font(null, Font.PLAIN, 72));
            btn.setBackground(panelBackground);
        } else {
            btn.setFont(new Font(null, Font.PLAIN, 18));
        }
        if (isSelected) {
            btn.setBackground(selectionBackground);
            btn.setForeground(selectionForeground);
        }
        if (cellHasFocus) {
            btn.setBorder(BorderFactory.createLineBorder(selectionForeground, 1, true));
        }

        return btn;
    }

}

