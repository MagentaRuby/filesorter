package me.rubyjay.apps.file_sorter;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.ValueType;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.yaml.YAMLConfigurationLoader;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class ConfigFile implements ConfigurationNode {

    private final YAMLConfigurationLoader loader;
    private final ConfigurationNode node;

    public ConfigFile(YAMLConfigurationLoader loader, ConfigurationNode node) {
        this.loader = loader;
        this.node = node;
    }

    //<editor-fold desc="Implementation">
    @Override
    public @Nullable Object getKey() {
        return node.getKey();
    }

    @Override
    public Object[] getPath() {
        return node.getPath();
    }

    @Override
    public @Nullable ConfigurationNode getParent() {
        return node.getParent();
    }

    @Override
    public @NonNull ConfigurationNode getNode(@NonNull Object... path) {
        return node.getNode(path);
    }

    @Override
    public boolean isVirtual() {
        return node.isVirtual();
    }

    @Override
    public @NonNull ConfigurationOptions getOptions() {
        return node.getOptions();
    }

    @Override @Deprecated
    public @NonNull ValueType getValueType() {
        return node.getValueType();
    }

    @Override
    public @NonNull List<? extends ConfigurationNode> getChildrenList() {
        return node.getChildrenList();
    }

    @Override
    public @NonNull Map<Object, ? extends ConfigurationNode> getChildrenMap() {
        return node.getChildrenMap();
    }

    @Override
    public Object getValue(@Nullable Object def) {
        return node.getValue(def);
    }

    @Override
    public Object getValue(@NonNull Supplier<Object> defSupplier) {
        return node.getValue(defSupplier);
    }

    @Override
    public <T> T getValue(@NonNull Function<Object, T> transformer, @Nullable T def) {
        return node.getValue(transformer, def);
    }

    @Override
    public <T> T getValue(@NonNull Function<Object, T> transformer, @NonNull Supplier<T> defSupplier) {
        return node.getValue(transformer, defSupplier);
    }

    @Override
    public @NonNull <T> List<T> getList(@NonNull Function<Object, T> transformer) {
        return node.getList(transformer);
    }

    @Override
    public <T> List<T> getList(@NonNull Function<Object, T> transformer, @Nullable List<T> def) {
        return node.getList(transformer, def);
    }

    @Override
    public <T> List<T> getList(@NonNull Function<Object, T> transformer, @NonNull Supplier<List<T>> defSupplier) {
        return node.getList(transformer, defSupplier);
    }

    @Override
    public <T> List<T> getList(@NonNull TypeToken<T> type, @Nullable List<T> def) throws ObjectMappingException {
        return node.getList(type, def);
    }

    @Override
    public <T> List<T> getList(@NonNull TypeToken<T> type, @NonNull Supplier<List<T>> defSupplier) throws ObjectMappingException {
        return node.getList(type, defSupplier);
    }

    @Override
    public <T> T getValue(@NonNull TypeToken<T> type, T def) throws ObjectMappingException {
        return node.getValue(type, def);
    }

    @Override
    public <T> T getValue(@NonNull TypeToken<T> type, @NonNull Supplier<T> defSupplier) throws ObjectMappingException {
        return node.getValue(type, defSupplier);
    }

    @Override
    public @NonNull ConfigurationNode setValue(@Nullable Object value) {
        return node.setValue(value);
    }

    @Override
    public @NonNull ConfigurationNode mergeValuesFrom(@NonNull ConfigurationNode other) {
        return node.mergeValuesFrom(other);
    }

    @Override
    public boolean removeChild(@NonNull Object key) {
        return node.removeChild(key);
    }

    @Override
    public @NonNull ConfigurationNode getAppendedNode() {
        return node.getAppendedNode();
    }

    @Override
    public @NonNull ConfigurationNode copy() {
        return node.copy();
    }
    //</editor-fold>

    public YAMLConfigurationLoader getLoader() {
        return loader;
    }

    public ConfigurationNode unwrap() {
        return node;
    }

    public void save() {
        try {
            loader.save(node);
        } catch (IOException ex) {
            FileSorterApp.handleException(ex);
        }
    }

}
