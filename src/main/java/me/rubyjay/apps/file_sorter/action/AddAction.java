package me.rubyjay.apps.file_sorter.action;

import me.rubyjay.apps.file_sorter.FileSorterApp;
import me.rubyjay.apps.file_sorter.file_type.FileType;
import me.rubyjay.apps.file_sorter.forms.EditActionForm;
import me.rubyjay.apps.file_sorter.forms.MainForm;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.io.File;

public class AddAction implements Action {

    @Nullable
    @Override
    public Icon getIcon() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "+";
    }

    @NotNull
    @Override
    public String getTooltip() {
        return "Add action...";
    }

    @Override
    public boolean matchesType(FileType type) {
        return true;
    }

    @Override
    public boolean execute(MainForm mainForm, FileType type, File file) {
        final int index = mainForm.listLocations.getModel().getSize();
        final BasicAction action = new BasicAction(type.getID());

        new EditActionForm(mainForm, index, action, false) {
            @Override
            public boolean onSubmit(BasicAction action) {
                return FileSorterApp.getActionManager().addAction(mainForm.getSelectedFileType(), action);
            }

            @Override
            public boolean onDelete(int index, BasicAction action) {
                return FileSorterApp.getActionManager().deleteAt(action.getFileTypeID(), index);
            }
        };
        return true;
    }

}
