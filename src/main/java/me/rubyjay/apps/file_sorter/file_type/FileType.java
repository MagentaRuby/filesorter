package me.rubyjay.apps.file_sorter.file_type;

import com.google.common.reflect.TypeToken;
import me.rubyjay.apps.file_sorter.ArchiveUtil;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class FileType {

    @NotNull
    private final String id;
    @Nullable
    private final String displayName;
    @Nullable
    private final String mimeType;
    @Nullable
    private final String fileMatches;
    @NotNull
    private final List<String> archiveContains;

    public FileType(@NotNull String id, @Nullable String displayName, @Nullable String mimeType, @Nullable String fileMatches, @NotNull List<String> archiveContains) {
        this.id = id;
        this.displayName = displayName;
        this.mimeType = mimeType;
        this.fileMatches = fileMatches;
        this.archiveContains = archiveContains;
    }

    @SuppressWarnings("UnstableApiUsage")
    public static FileType load(ConfigurationNode c) throws ObjectMappingException {
        final String id = Objects.requireNonNull(c.getKey()).toString();
        final String displayName = c.getNode("display-name").getString();
        final String mimeType = c.getNode("mime-type").getString();
        final String fileMatches = c.getNode("file-name").getString();
        final List<String> archiveContains = c.getNode("archive-contains").getList(TypeToken.of(String.class));

        return new FileType(id, displayName, mimeType, fileMatches, archiveContains);
    }

    public static FileType of(String mimeType) {
        return new FileType(mimeType, mimeType, mimeType, null, Collections.emptyList());
    }

    @NotNull
    public String getID() {
        return id;
    }

    @Nullable
    public String getDisplayName() {
        return displayName;
    }

    @Nullable
    public String getMimeType() {
        return mimeType;
    }

    @Nullable
    public String getFileMatches() {
        return fileMatches;
    }

    @NotNull
    public List<String> getArchiveContains() {
        return archiveContains;
    }

    public boolean matches(File file, String mimeType) {
        return (this.mimeType == null || mimeType.matches(this.mimeType))
            && (this.fileMatches == null || file.getName().matches(this.fileMatches))
            && (archiveContains.isEmpty() || ArchiveUtil.containsFileNames(file, archiveContains));
    }

}
