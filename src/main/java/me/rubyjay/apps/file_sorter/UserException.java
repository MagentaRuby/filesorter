package me.rubyjay.apps.file_sorter;

public class UserException extends Exception {

    public UserException(String message) {
        super(message);
    }

    public UserException(String message, Object... args) {
        super(String.format(message, args));
    }

}
