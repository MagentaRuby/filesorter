package me.rubyjay.apps.file_sorter.file_type;

import me.rubyjay.apps.file_sorter.ConfigFile;
import me.rubyjay.apps.file_sorter.FileSorterApp;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class FileTypeManager implements Iterable<FileType> {

    private ConfigFile conf;
    private final List<FileType> fileTypes;

    public FileTypeManager() {
        this.fileTypes = new LinkedList<>();
    }

    public void load(ConfigFile conf) {
        this.conf = conf;
        for (ConfigurationNode c : conf.getChildrenMap().values()) {
            try {
                fileTypes.add(FileType.load(c));
            } catch (ObjectMappingException ex) {
                FileSorterApp.handleException(ex);
            }
        }
    }

    @NotNull
    @Override
    public Iterator<FileType> iterator() {
        return fileTypes.iterator();
    }

}
