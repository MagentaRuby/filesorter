package me.rubyjay.apps.file_sorter.forms;

import me.rubyjay.apps.file_sorter.forms.settings_panes.FileTypesPane;
import me.rubyjay.apps.file_sorter.forms.settings_panes.GeneralPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

public class SettingsForm extends JDialog {

    private JPanel panelMain;
    private JTabbedPane tabbedPane;

    private JPanel panelButtons;
    private JButton btnOK;
    private JButton btnCancel;

    public SettingsForm(MainForm owner) throws HeadlessException {
        super(owner, "Settings");

        panelMain = new JPanel(new BorderLayout());
        this.setContentPane(panelMain);

        tabbedPane = new JTabbedPane();
        panelMain.add(tabbedPane, BorderLayout.CENTER);

        tabbedPane.addTab("General", new GeneralPane(this));
        tabbedPane.addTab("File Types", new FileTypesPane(this));

        panelButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        panelMain.add(panelButtons, BorderLayout.SOUTH);

        btnOK = new JButton("OK");
        btnOK.setEnabled(false);
        btnOK.addActionListener(e -> onSubmit());
        SwingUtilities.getRootPane(this).setDefaultButton(btnOK);
        panelButtons.add(btnOK);

        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(e -> this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)));
        panelButtons.add(btnCancel);

        this.setSize(500, 600);
        this.setLocationRelativeTo(owner);
        this.setVisible(true);
    }

    private void onSubmit() {
        JOptionPane.showMessageDialog(this, "TODO");
    }

}
