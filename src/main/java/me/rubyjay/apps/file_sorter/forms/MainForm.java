package me.rubyjay.apps.file_sorter.forms;

import me.rubyjay.apps.file_sorter.ConfigFile;
import me.rubyjay.apps.file_sorter.FileSorterApp;
import me.rubyjay.apps.file_sorter.action.*;
import me.rubyjay.apps.file_sorter.action.Action;
import me.rubyjay.apps.file_sorter.file_type.FileType;
import ninja.leaping.configurate.ConfigurationNode;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;

public class MainForm extends JFrame implements WindowListener {

    private JPanel panelMain;
    private JPanel panelFile;
    private JLabel lblFileType;
    private JTextField txtFileName;
    private JButton btnSettings;
    private JScrollPane scrollLocations;
    public JList<Action> listLocations;
    private JPanel panelButtons;
    private JButton btnOK;
    private JButton btnCancel;

    private File selectedFile;
    private FileType selectedFileType;

    public MainForm() throws HeadlessException {
        final ConfigurationNode c = FileSorterApp.getMainConf().getNode("window");

        if (c.getNode("x").isEmpty()
         || c.getNode("y").isEmpty()
         || c.getNode("width").isEmpty()
         || c.getNode("height").isEmpty()) {
            this.setSize(800, 600);
        } else {
            this.setLocation(c.getNode("x").getInt(), c.getNode("y").getInt());
            this.setSize(c.getNode("width").getInt(), c.getNode("height").getInt());
        }

        panelMain = new JPanel(new BorderLayout());
        this.setContentPane(panelMain);

        panelFile = new JPanel(new BorderLayout());
        panelFile.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        panelMain.add(panelFile, BorderLayout.NORTH);

        lblFileType = new JLabel("No file selected.");
        lblFileType.setFont(new Font(null, Font.PLAIN, 18));
        lblFileType.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 8));
        panelFile.add(lblFileType, BorderLayout.WEST);

        txtFileName = new JTextField();
        txtFileName.setFont(new Font(null, Font.PLAIN, 18));
        panelFile.add(txtFileName, BorderLayout.CENTER);

        btnSettings = new JButton("Settings");
        btnSettings.addActionListener(e -> new SettingsForm(this));
        panelFile.add(btnSettings, BorderLayout.EAST);

        final Color colorLocations = UIManager.getColor("Panel.background").darker();

        listLocations = new JList<>();

        listLocations.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        listLocations.setCellRenderer(new ActionRenderer());
        listLocations.setVisibleRowCount(-1);
        listLocations.setFocusable(false);
        listLocations.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        listLocations.setBackground(colorLocations);
        listLocations.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));

        listLocations.addListSelectionListener(e -> {
            if (e.getValueIsAdjusting()) return;

            validateOK();

            List<Action> values = listLocations.getSelectedValuesList();

            for (int i = 0; i < values.size(); i++) {
                Action action = values.get(i);

                if (action instanceof DummyAction) {
                    listLocations.clearSelection();
                    return;
                } else if (!(action instanceof BasicAction)) {
                    action.execute(this, selectedFileType, selectedFile);
//                    JOptionPane.showMessageDialog(this, "Show add action dialog.");
                    listLocations.clearSelection();
                    return;
                }
            }
        });

        listLocations.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int index = listLocations.locationToIndex(e.getPoint());

                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (index >= 0 && e.getClickCount() == 2) onSubmit();
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    final Action action = listLocations.getModel().getElementAt(index);

                    if (action instanceof BasicAction) {
                        new EditActionForm(MainForm.this, index, (BasicAction) action, true) {
                            @Override
                            public boolean onSubmit(BasicAction action) {
                                return FileSorterApp.getActionManager().editAction(selectedFileType, index, action);
                            }

                            @Override
                            public boolean onDelete(int index, BasicAction action) {
                                return FileSorterApp.getActionManager().deleteAt(selectedFileType.getID(), index);
                            }
                        };
                    }
                }
            }
        });

        scrollLocations = new JScrollPane(listLocations);
        scrollLocations.setBackground(colorLocations);
        panelMain.add(scrollLocations, BorderLayout.CENTER);

        panelButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        panelMain.add(panelButtons, BorderLayout.SOUTH);

        btnOK = new JButton("OK");
        btnOK.setEnabled(false);
        btnOK.addActionListener(e -> onSubmit());
        SwingUtilities.getRootPane(this).setDefaultButton(btnOK);
        panelButtons.add(btnOK);

        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(e -> System.exit(0));
        panelButtons.add(btnCancel);

        this.addWindowListener(this);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void selectFile(@NotNull File file) {
        try {
            final String mimeType = Files.probeContentType(file.toPath());
            final FileType type = FileSorterApp.getFileType(file, mimeType);

            this.selectedFile = file;
            this.selectedFileType = type;

            setTitle(file.getName());

            if (type.getDisplayName() != null) {
                lblFileType.setText("(" + type.getDisplayName() + ")");
            } else {
                lblFileType.setText("(" + mimeType + ")");
            }
            lblFileType.setToolTipText(String.format("Display Name: %s, MIME Type: %s", type.getDisplayName(), mimeType));
            txtFileName.setText(file.getName());
            txtFileName.setEnabled(true);

            DefaultListModel<Action> listLocationsModel = new DefaultListModel<>();

            final Set<Action> actions = FileSorterApp.getActions(type);

            listLocationsModel.addAll(actions);
            listLocationsModel.addElement(new AddAction());
            listLocationsModel.addElement(new DummyAction());
            listLocations.setModel(listLocationsModel);

            validateOK();

        } catch (IOException ex) {
            FileSorterApp.handleException(ex);
            selectNoFile();
        }
    }

    public void selectNoFile() {
        this.selectedFile = null;
        this.selectedFileType = null;

        lblFileType.setText("No file selected.");
        txtFileName.setText("");
        txtFileName.setEnabled(false);

        DefaultListModel<Action> listLocationsModel = new DefaultListModel<>();

        listLocationsModel.addElement(new BrowseAction());
        listLocations.setModel(listLocationsModel);

        validateOK();
    }

    private void validateOK() {
        final List<Action> selected = listLocations.getSelectedValuesList();
        boolean valid = false;

        if (selected.size() > 0) {
            valid = true;
            for (Action action : selected) {
                if (!(action instanceof BasicAction)) {
                    valid = false;
                }
            }
        }
        btnOK.setEnabled(valid);
    }

    private void onSubmit() {
        boolean result = true;

        for (Action action : listLocations.getSelectedValuesList()) {
            if (action instanceof BasicAction) {
                if (!action.execute(this, selectedFileType, selectedFile)) {
                    result = false;
                }
            }
        }
        if (result) System.exit(0);
    }

    //<editor-fold desc="Window Events">
    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        final ConfigFile mainConf = FileSorterApp.getMainConf();
        final @NonNull ConfigurationNode c = mainConf.getNode("window");

        final Window window = windowEvent.getWindow();

        final Point l = window.getLocation();

        c.getNode("x").setValue(l.x);
        c.getNode("y").setValue(l.y);

        final Dimension size = window.getSize();

        c.getNode("width").setValue(size.width);
        c.getNode("height").setValue(size.height);

        mainConf.save();
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }
    //</editor-fold>

    public File getSelectedFile() {
        return selectedFile;
    }

    public FileType getSelectedFileType() {
        return selectedFileType;
    }

}
