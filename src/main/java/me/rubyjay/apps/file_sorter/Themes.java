package me.rubyjay.apps.file_sorter;

import com.formdev.flatlaf.IntelliJTheme;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public final class Themes {

    private Themes() {}

    public enum Category {
        NORMAL {
            @NotNull
            @Override
            public Map<String, Class<? extends IntelliJTheme.ThemeLaf>> getThemes() {
                return themes;
            }

            @NotNull
            @Override
            public IntelliJTheme.ThemeLaf getTheme(@NotNull String themeName) throws UserException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
                final Class<? extends IntelliJTheme.ThemeLaf> aClass = themes.get(themeName);

                if (aClass == null) throw new UserException("Theme does not exist: %s", themeName);

                return aClass.getConstructor().newInstance();
            }
        },
        MATERIAL {
            @NotNull
            @Override
            public Map<String, Class<? extends IntelliJTheme.ThemeLaf>> getThemes() {
                return materialThemes;
            }

            @NotNull
            @Override
            public IntelliJTheme.ThemeLaf getTheme(@NotNull String themeName) throws UserException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
                final Class<? extends IntelliJTheme.ThemeLaf> aClass = materialThemes.get(themeName);

                if (aClass == null) throw new UserException("Material theme does not exist: %s", themeName);

                return aClass.getConstructor().newInstance();
            }
        };

        @NotNull
        public abstract Map<String, Class<? extends IntelliJTheme.ThemeLaf>> getThemes();

        @NotNull
        public abstract IntelliJTheme.ThemeLaf getTheme(@NotNull String themeName) throws UserException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;
    }

    private static final Map<String, Class<? extends IntelliJTheme.ThemeLaf>> themes
        = ImmutableMap.<String, Class<? extends IntelliJTheme.ThemeLaf>>builder()
            .put("Arc",
                    com.formdev.flatlaf.intellijthemes.FlatArcIJTheme.class)
            .put("Arc - Orange",
                    com.formdev.flatlaf.intellijthemes.FlatArcOrangeIJTheme.class)
            .put("Arc Dark",
                    com.formdev.flatlaf.intellijthemes.FlatArcDarkIJTheme.class)
            .put("Arc Dark - Orange",
                    com.formdev.flatlaf.intellijthemes.FlatArcDarkOrangeIJTheme.class)
            .put("Carbon",
                    com.formdev.flatlaf.intellijthemes.FlatCarbonIJTheme.class)
            .put("Cobalt 2",
                    com.formdev.flatlaf.intellijthemes.FlatCobalt2IJTheme.class)
            .put("Cyan light",
                    com.formdev.flatlaf.intellijthemes.FlatCyanLightIJTheme.class)
            .put("Dark Flat",
                    com.formdev.flatlaf.intellijthemes.FlatDarkFlatIJTheme.class)
            .put("Dark purple",
                    com.formdev.flatlaf.intellijthemes.FlatDarkPurpleIJTheme.class)
            .put("Dracula",
                    com.formdev.flatlaf.intellijthemes.FlatDraculaIJTheme.class)
            .put("Gradianto Dark Fuchsia",
                    com.formdev.flatlaf.intellijthemes.FlatGradiantoDarkFuchsiaIJTheme.class)
            .put("Gradianto Deep Ocean",
                    com.formdev.flatlaf.intellijthemes.FlatGradiantoDeepOceanIJTheme.class)
            .put("Gradianto Midnight Blue",
                    com.formdev.flatlaf.intellijthemes.FlatGradiantoMidnightBlueIJTheme.class)
            .put("Gradianto Nature Green",
                    com.formdev.flatlaf.intellijthemes.FlatGradiantoNatureGreenIJTheme.class)
            .put("Gray",
                    com.formdev.flatlaf.intellijthemes.FlatGrayIJTheme.class)
            .put("Gruvbox Dark Hard",
                    com.formdev.flatlaf.intellijthemes.FlatGruvboxDarkHardIJTheme.class)
            .put("Gruvbox Dark Medium",
                    com.formdev.flatlaf.intellijthemes.FlatGruvboxDarkMediumIJTheme.class)
            .put("Gruvbox Dark Soft",
                    com.formdev.flatlaf.intellijthemes.FlatGruvboxDarkSoftIJTheme.class)
            .put("Hiberbee Dark",
                    com.formdev.flatlaf.intellijthemes.FlatHiberbeeDarkIJTheme.class)
            .put("High contrast",
                    com.formdev.flatlaf.intellijthemes.FlatHighContrastIJTheme.class)
            .put("Light Flat",
                    com.formdev.flatlaf.intellijthemes.FlatLightFlatIJTheme.class)
            .put("Material Design Dark",
                    com.formdev.flatlaf.intellijthemes.FlatMaterialDesignDarkIJTheme.class)
            .put("Monocai",
                    com.formdev.flatlaf.intellijthemes.FlatMonocaiIJTheme.class)
            .put("Nord",
                    com.formdev.flatlaf.intellijthemes.FlatNordIJTheme.class)
            .put("One Dark",
                    com.formdev.flatlaf.intellijthemes.FlatOneDarkIJTheme.class)
            .put("Solarized Dark",
                    com.formdev.flatlaf.intellijthemes.FlatSolarizedDarkIJTheme.class)
            .put("Solarized Light",
                    com.formdev.flatlaf.intellijthemes.FlatSolarizedLightIJTheme.class)
            .put("Spacegray",
                    com.formdev.flatlaf.intellijthemes.FlatSpacegrayIJTheme.class)
            .put("Vuesion",
                    com.formdev.flatlaf.intellijthemes.FlatVuesionIJTheme.class)
            .build();

    private static final Map<String, Class<? extends IntelliJTheme.ThemeLaf>> materialThemes
        = ImmutableMap.<String, Class<? extends IntelliJTheme.ThemeLaf>>builder()
            .put("Arc Dark",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatArcDarkIJTheme.class)
            .put("Arc Dark Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatArcDarkContrastIJTheme.class)
            .put("Atom One Dark",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatAtomOneDarkIJTheme.class)
            .put("Atom One Dark Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatAtomOneDarkContrastIJTheme.class)
            .put("Atom One Light",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatAtomOneLightIJTheme.class)
            .put("Atom One Light Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatAtomOneLightContrastIJTheme.class)
            .put("Dracula",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatDraculaIJTheme.class)
            .put("Dracula Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatDraculaContrastIJTheme.class)
            .put("GitHub",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatGitHubIJTheme.class)
            .put("GitHub Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatGitHubContrastIJTheme.class)
            .put("Light Owl",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatLightOwlIJTheme.class)
            .put("Light Owl Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatLightOwlContrastIJTheme.class)
            .put("Material Darker",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialDarkerIJTheme.class)
            .put("Material Darker Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialDarkerContrastIJTheme.class)
            .put("Material Deep Ocean",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialDeepOceanIJTheme.class)
            .put("Material Deep Ocean Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialDeepOceanContrastIJTheme.class)
            .put("Material Lighter",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialLighterIJTheme.class)
            .put("Material Lighter Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialLighterContrastIJTheme.class)
            .put("Material Oceanic",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialOceanicIJTheme.class)
            .put("Material Oceanic Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialOceanicContrastIJTheme.class)
            .put("Material Palenight",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialPalenightIJTheme.class)
            .put("Material Palenight Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialPalenightContrastIJTheme.class)
            .put("Monokai Pro",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMonokaiProIJTheme.class)
            .put("Monokai Pro Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMonokaiProContrastIJTheme.class)
            .put("Moonlight",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMoonlightIJTheme.class)
            .put("Moonlight Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMoonlightContrastIJTheme.class)
            .put("Night Owl",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatNightOwlIJTheme.class)
            .put("Night Owl Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatNightOwlContrastIJTheme.class)
            .put("Solarized Dark",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatSolarizedDarkIJTheme.class)
            .put("Solarized Dark Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatSolarizedDarkContrastIJTheme.class)
            .put("Solarized Light",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatSolarizedLightIJTheme.class)
            .put("Solarized Light Contrast",
                    com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatSolarizedLightContrastIJTheme.class)
            .build();

}
