package me.rubyjay.apps.file_sorter;

import com.formdev.flatlaf.IntelliJTheme;
import com.google.common.reflect.TypeToken;
import me.rubyjay.apps.file_sorter.action.Action;
import me.rubyjay.apps.file_sorter.action.ActionManager;
import me.rubyjay.apps.file_sorter.action.BrowseAction;
import me.rubyjay.apps.file_sorter.file_type.FileType;
import me.rubyjay.apps.file_sorter.file_type.FileTypeManager;
import me.rubyjay.apps.file_sorter.forms.MainForm;
import ninja.leaping.configurate.yaml.YAMLConfigurationLoader;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.jetbrains.annotations.NotNull;
import org.yaml.snakeyaml.DumperOptions;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static javax.swing.JOptionPane.ERROR_MESSAGE;

public class FileSorterApp {

    private static final int DEFAULT_BUFFER_SIZE = 8192;

    private static File workingDir;

    private static ConfigFile mainConf;
    private static ActionManager actionManager;
    private static FileTypeManager fileTypeManager;

    private static Themes.@Nullable Category themeCategory;
    private static String themeName;

    private static MainForm mainForm;

    public static void main(String[] args) {
        workingDir = Paths.get("").toAbsolutePath().toFile();

        if (!loadAllConfigs()) return;

        final IntelliJTheme.ThemeLaf theme;

        try {
            themeCategory = mainConf.getNode("theme", "category").getValue(TypeToken.of(Themes.Category.class));

            if (themeCategory == null) throw new UserException("Theme category does not exist: %s",
                    mainConf.getNode("theme", "category").getString(""));

            themeName = mainConf.getNode("theme", "name").getString("");

            theme = themeCategory.getTheme(themeName);

            UIManager.setLookAndFeel(theme);
        } catch (Exception ex) {
            handleException(ex);
        }

        mainForm = new MainForm();

        if (args.length <= 0) {
            BrowseAction.execute(mainForm);
            return;
        }

        File file = new File(args[0]);

        if (file.exists()) {
            mainForm.selectFile(file);
        } else {
            JOptionPane.showMessageDialog(mainForm, "File does not exist.", "Error", ERROR_MESSAGE);
            mainForm.selectNoFile();
        }
    }

    public static boolean loadAllConfigs() {
        final Optional<ConfigFile> mainConf = loadConfig("config.yml");

        if (!mainConf.isPresent()) return false;
        FileSorterApp.mainConf = mainConf.get();

        final Optional<ConfigFile> actionsConf = loadConfig("actions.yml");

        if (!actionsConf.isPresent()) return false;

        actionManager = new ActionManager();
        actionManager.load(actionsConf.get());

        final Optional<ConfigFile> fileTypesConf = loadConfig("file-types.yml");

        if (!fileTypesConf.isPresent()) return false;

        fileTypeManager = new FileTypeManager();
        fileTypeManager.load(fileTypesConf.get());

        return true;
    }

    private static Optional<ConfigFile> loadConfig(String filename) {
        try {
            final File file = new File(workingDir, filename);

            if (!file.exists()) {
                final InputStream inputStream = FileSorterApp.class.getResourceAsStream("/" + filename);

                if (inputStream == null) {
                    handleException(new IOException(filename + " does not exist in resources!"));
                    return Optional.empty();
                }

                try (FileOutputStream outputStream = new FileOutputStream(file, false)) {
                    int read;
                    byte[] bytes = new byte[DEFAULT_BUFFER_SIZE];
                    while ((read = inputStream.read(bytes)) != -1) {
                        outputStream.write(bytes, 0, read);
                    }
                }
            }
            final YAMLConfigurationLoader loader = YAMLConfigurationLoader.builder()
                    .setFile(file)
                    .setFlowStyle(DumperOptions.FlowStyle.BLOCK)
                    .setIndent(2)
                    .build();

            return Optional.of(
                    loader
                        .load())
                    .map(n -> new ConfigFile(loader, n));
        } catch (IOException | SecurityException ex) {
            handleException(ex);
            return Optional.empty();
        }
    }

    public static Set<Action> getActions(FileType type) {
        final Set<Action> actions = new TreeSet<>(Comparator.comparing(Action::getText));

        for (Action action : actionManager) {
            if (action.matchesType(type)) {
                actions.add(action);
            }
        }

        return actions;
    }

    public static FileType getFileType(File file, String mimeType) throws IOException {
        if (mimeType == null) {
            return FileType.of("unknown");
        }

        for (FileType fileType : fileTypeManager) {
            if (fileType.matches(file, mimeType)) return fileType;
        }
        return FileType.of(mimeType);
    }

    public static FileType getFileType(String id) throws IOException {
        if (id == null) {
            return FileType.of("unknown");
        }

        for (FileType fileType : fileTypeManager) {
            if (fileType.getID().equalsIgnoreCase(id)) return fileType;
        }
        return FileType.of(id);
    }

    public static void handleException(Throwable ex) {
        if (ex instanceof UserException) {
            JOptionPane.showMessageDialog(mainForm, ex.getMessage(), "Error", ERROR_MESSAGE);
            return;
        }

        final String stackTrace = Arrays.stream(ex.getStackTrace())
                .map(e -> "    " + e.toString())
                .collect(Collectors.joining("\n"));
        JOptionPane.showMessageDialog(mainForm, ex.toString() + "\n" + stackTrace, "Error", ERROR_MESSAGE);
    }

    public static ConfigFile getMainConf() {
        return mainConf;
    }

    public static File getWorkingDir() {
        return workingDir;
    }

    public static ActionManager getActionManager() {
        return actionManager;
    }

    public static FileTypeManager getFileTypeManager() {
        return fileTypeManager;
    }

    @NotNull
    public static Themes.Category getThemeCategory() {
        if (themeCategory == null) throw new IllegalStateException();
        return themeCategory;
    }

    @NotNull
    public static String getThemeName() {
        if (themeName == null) throw new IllegalStateException();
        return themeName;
    }

}
