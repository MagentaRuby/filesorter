package me.rubyjay.apps.file_sorter.action;

import me.rubyjay.apps.file_sorter.file_type.FileType;
import me.rubyjay.apps.file_sorter.forms.MainForm;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.io.File;

public class DummyAction implements Action {

    @Nullable
    @Override
    public Icon getIcon() {
        return null;
    }

    @NotNull
    @Override
    public String getText() {
        return "";
    }

    @NotNull
    @Override
    public String getTooltip() {
        return "";
    }

    @Override
    public boolean matchesType(FileType type) {
        return false;
    }

    @Override
    public boolean execute(MainForm mainForm, FileType type, File file) {
        throw new UnsupportedOperationException();
    }

}
