package me.rubyjay.apps.file_sorter.action;

import com.google.common.reflect.TypeToken;
import me.rubyjay.apps.file_sorter.ConfigFile;
import me.rubyjay.apps.file_sorter.FileSorterApp;
import me.rubyjay.apps.file_sorter.file_type.FileType;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class ActionManager implements Iterable<Action> {

    private ConfigFile conf;
    private final List<Action> actions;

    public ActionManager() {
        this.actions = new LinkedList<>();
    }

    public void load(ConfigFile conf) {
        this.conf = conf;
        for (ConfigurationNode cc : conf.getChildrenMap().values()) {
            final String fileTypeID = Objects.requireNonNull(cc.getKey()).toString();

            for (ConfigurationNode c : cc.getChildrenList()) {
                actions.add(Action.load(fileTypeID, c));
            }
        }
    }

    @NotNull
    @Override
    public Iterator<Action> iterator() {
        return actions.iterator();
    }

    public boolean addAction(FileType type, BasicAction action) {
        try {
            final ConfigurationNode node = conf.getNode(type.getID());
            final List<ConfigurationNode> list = node.getList(TypeToken.of(ConfigurationNode.class));

            list.add(Action.export(action, conf.getLoader().createEmptyNode()));
            node.setValue(list);
            conf.save();
            return true;
        } catch (ObjectMappingException ex) {
            FileSorterApp.handleException(ex);
            return false;
        }
    }

    public boolean editAction(FileType type, int index, BasicAction action) {
        try {
            final ConfigurationNode node = conf.getNode(type.getID());
            final List<ConfigurationNode> list = node.getList(TypeToken.of(ConfigurationNode.class));

            list.remove(index);
            list.add(index, Action.export(action, conf.getLoader().createEmptyNode()));
            node.setValue(list);
            conf.save();
            return true;
        } catch (ObjectMappingException ex) {
            FileSorterApp.handleException(ex);
            return false;
        }
    }

    public boolean deleteAt(String fileTypeID, int index) {
        try {
            final ConfigurationNode node = conf.getNode(fileTypeID);
            final List<ConfigurationNode> list = node.getList(TypeToken.of(ConfigurationNode.class));

            list.remove(index);
            node.setValue(list);
            conf.save();
            return true;
        } catch (ObjectMappingException ex) {
            FileSorterApp.handleException(ex);
            return false;
        }
    }

}
