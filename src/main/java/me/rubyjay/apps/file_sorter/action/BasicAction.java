package me.rubyjay.apps.file_sorter.action;

import me.rubyjay.apps.file_sorter.FileSorterApp;
import me.rubyjay.apps.file_sorter.file_type.FileType;
import me.rubyjay.apps.file_sorter.forms.MainForm;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class BasicAction implements Action {

    @NotNull
    private String fileTypeID;
    @Nullable
    private Icon icon;
    @NotNull
    private String displayName;
    @Nullable
    private String destination;
    @Nullable
    private String command;

    private boolean showDestination;

    private boolean showCommand;

    public BasicAction(@NotNull String fileTypeID, @Nullable Icon icon, @NotNull String displayName, @Nullable String destination, @Nullable String command, boolean showDestination, boolean showCommand) {
        this.fileTypeID = fileTypeID;
        this.icon = icon;
        this.displayName = displayName;
        this.destination = destination;
        this.command = command;
        this.showDestination = showDestination;
        this.showCommand = showCommand;
    }

    public BasicAction(String fileTypeID) {
        this.fileTypeID = fileTypeID;
        this.icon = null;
        this.displayName = "";
        this.destination = "";
        this.command = "";
        this.showDestination = false;
        this.showCommand = false;
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return icon;
    }

    @Override
    @NotNull
    public String getText() {
        final StringBuilder b = new StringBuilder("<html><b>")
                .append(displayName).append("</b>");

        if (destination != null && showDestination) {
            b.append("<br>").append(destination);
        }

        if (command != null && showCommand) {
            b.append("<br>").append(command);
        }

        return b.append("</html>").toString();
    }

    @Override
    @NotNull
    public String getTooltip() {
        final StringBuilder b = new StringBuilder("<html><b>")
                .append(displayName).append("</b>");

        if (destination != null) {
            b.append("<br>").append(destination);
        }

        if (command != null) {
            b.append("<br>").append(command);
        }

        return b.append("</html>").toString();
    }

    public boolean hasDestination() {
        return destination != null && !destination.isEmpty();
    }

    @Nullable
    public String getDestination() {
        return destination;
    }

    public boolean hasCommand() {
        return command != null && !command.isEmpty();
    }

    @Nullable
    public String getCommand() {
        return command;
    }

    @NotNull
    public String getFileTypeID() {
        return fileTypeID;
    }

    public void setFileTypeID(@NotNull String fileTypeID) {
        this.fileTypeID = fileTypeID;
    }

    public void setIcon(@Nullable Icon icon) {
        this.icon = icon;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(@NotNull String displayName) {
        this.displayName = displayName;
    }

    public void setDestination(@Nullable String destination) {
        this.destination = destination;
    }

    public void setCommand(@Nullable String command) {
        this.command = command;
    }

    public boolean showDestination() {
        return showDestination;
    }

    public void setShowDestination(boolean showDestination) {
        this.showDestination = showDestination;
    }

    public boolean showCommand() {
        return showCommand;
    }

    public void setShowCommand(boolean showCommand) {
        this.showCommand = showCommand;
    }

    @Override
    public boolean matchesType(FileType type) {
        return this.fileTypeID.equalsIgnoreCase(type.getID());
    }

    @Override
    public boolean execute(MainForm mainForm, FileType type, File file) {
        if (destination != null && !destination.isEmpty()) {
            final File oldFile = file;
            final File newFile = new File(destination, file.getName());

            try {
                Files.copy(oldFile.toPath(), newFile.toPath());

                file = newFile;
            } catch (IOException ex) {
                FileSorterApp.handleException(ex);
                return false;
            }
        }

        if (command != null && !command.isEmpty()) {
            final String command = this.command.replace("%f", "\"" + file.getAbsolutePath() + "\"");

            try {
                Runtime.getRuntime().exec(command);
            } catch (IOException ex) {
                FileSorterApp.handleException(ex);
                return false;
            }
        }

        return true;
    }

}
