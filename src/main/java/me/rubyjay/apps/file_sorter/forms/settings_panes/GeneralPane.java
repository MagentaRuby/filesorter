package me.rubyjay.apps.file_sorter.forms.settings_panes;

import com.formdev.flatlaf.IntelliJTheme;
import me.rubyjay.apps.file_sorter.FileSorterApp;
import me.rubyjay.apps.file_sorter.Themes;
import me.rubyjay.apps.file_sorter.forms.SettingsForm;

import javax.swing.*;
import java.awt.*;
import java.util.function.Consumer;

import static me.rubyjay.apps.file_sorter.Themes.Category.MATERIAL;
import static me.rubyjay.apps.file_sorter.Themes.Category.NORMAL;

public class GeneralPane extends JScrollPane {

    private final SettingsForm settingsForm;

    private JPanel panelContent;
    private JPanel panelTheme;
    private JLabel lblCategory;
    private ButtonGroup grpCategory;
    private JRadioButton radCategoryNormal;
    private JRadioButton radCategoryMaterial;
    private JLabel lblThemeName;
    private JComboBox<String> cmbThemeName;

    private Themes.Category themeCategory;
    private String themeName;

    public GeneralPane(SettingsForm settingsForm) {
        super(new JPanel());
        this.settingsForm = settingsForm;

        panelContent = (JPanel) this.getViewport().getView();
        panelContent.setLayout(new BoxLayout(panelContent, BoxLayout.Y_AXIS));

        panelTheme = new JPanel(new GridBagLayout());
        panelTheme.setBorder(BorderFactory.createTitledBorder("Theme"));
        panelContent.add(panelTheme);

        lblCategory = new JLabel("Category");
        lblCategory.setHorizontalAlignment(JLabel.LEFT);
        add(panelTheme, lblCategory, c -> {
            c.gridx = 0;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        grpCategory = new ButtonGroup();

        radCategoryNormal = new JRadioButton("Normal");
        radCategoryNormal.addActionListener(e -> selectTheme(NORMAL, null));
        add(panelTheme, radCategoryNormal, c -> {
            c.gridx = 1;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });
        grpCategory.add(radCategoryNormal);

        radCategoryMaterial = new JRadioButton("Material");
        radCategoryMaterial.addActionListener(e -> selectTheme(MATERIAL, null));
        add(panelTheme, radCategoryMaterial, c -> {
            c.gridx = 2;
            c.gridy = 0;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });
        grpCategory.add(radCategoryMaterial);

        lblThemeName = new JLabel("Name");
        lblThemeName.setHorizontalAlignment(JLabel.LEFT);
        add(panelTheme, lblThemeName, c -> {
            c.gridx = 0;
            c.gridy = 1;
            c.gridwidth = 1;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        cmbThemeName = new JComboBox<>();
        cmbThemeName.addActionListener(e -> activateTheme(themeCategory, (String) cmbThemeName.getSelectedItem()));
        add(panelTheme, cmbThemeName, c -> {
            c.gridx = 1;
            c.gridy = 1;
            c.gridwidth = 2;
            c.gridheight = 1;
            c.insets = new Insets(5, 5, 5, 5);
            c.fill = GridBagConstraints.HORIZONTAL;
        });

        panelContent.add(Box.createVerticalGlue());

        selectTheme(FileSorterApp.getThemeCategory(), FileSorterApp.getThemeName());
    }

    private void add(JPanel panel, JComponent component, Consumer<GridBagConstraints> gridBagConstraintsConsumer) {
        final GridBagConstraints c = new GridBagConstraints();

        gridBagConstraintsConsumer.accept(c);
        panel.add(component, c);
    }

    public void selectTheme(Themes.Category category, String name) {
        this.themeCategory = category;
        this.themeName = name;

        switch (category) {
            case NORMAL:
                radCategoryNormal.setSelected(true);
                break;
            case MATERIAL:
                radCategoryMaterial.setSelected(true);
                break;
        }
        final DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>();

        model.addAll(category.getThemes().keySet());
        cmbThemeName.setModel(model);
        cmbThemeName.setSelectedItem(name);
    }

    private void activateTheme(Themes.Category category, String name) {
        this.themeCategory = category;
        this.themeName = name;

        if (category != null && name != null) {
            try {
                IntelliJTheme.ThemeLaf theme = themeCategory.getTheme(themeName);

                UIManager.setLookAndFeel(theme);
            } catch (Exception ex) {
                FileSorterApp.handleException(ex);
            }
        }
    }

}
